FROM node:lts-alpine 
LABEL maintainer="Sukipli <kipli176@gmail.com>"
RUN apk add --no-cache tini
WORKDIR /usr/src/app
COPY . ./  
COPY .env* ./  
RUN chown node:node .
USER node
COPY package*.json ./
RUN npm install  
EXPOSE 3000
# ENTRYPOINT [ "/sbin/tini","--", "node", "server.js" ]
CMD [ "node", "server.js" ]

