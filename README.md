# Setting up SkyX NFT API

```
Required services:
1. MongoDB for saving nft metadata and blockchain transaction. 
2. MIN.io for object storage. (use domain like https://file.skyxtoken.io )
3. https://infura.io for interacting with Ethereum nodes. (Need to create an account)
4. https://bsc-dataseed.binance.org for interacting with Smartchain node. (no need to create an account)
```

```
Development:
BSC_CONTRACT_ADDRESS={smart_chain_contract_address}
BSC_NETWORK=https://data-seed-prebsc-1-s1.binance.org:8545

ETH_CONTRACT_ADDRESS={ethereum-contract-address}
ETH_NETWORK=https://ropsten.infura.io/v3/{your-project-id}
```

```
Development:
BSC_CONTRACT_ADDRESS={smart-chain-contract-address}
BSC_NETWORK=https://bsc-dataseed.binance.org

ETH_CONTRACT_ADDRESS={ethereum_contract_address}
ETH_NETWORK=https://mainnet.infura.io/v3/{your-project-id}
```

```
Note:
Key KBSC_CONTRACT_ADDRESS & BSC_NETWORK don't change the values before final launch
to fix ssl error https://stackoverflow.com/questions/17200391/nodejs-unable-to-verify-leaf-signature/21910240
```
