require('custom-env').env('development')
const Minio = require("minio")
const config = require("../config/db.config.js")
const Web3 = require('web3')
const AWS = require('aws-sdk');
const md = require("../models")
const Metadata = md.metadata
const Tx = md.transaction
const Sale = md.sale
const Collection = md.collection
const Counter = md.counter
const {
   v4: uuidv4
} = require('uuid')
const {
   Validator
} = require('node-input-validator')
const abiDecoder = require('abi-decoder')
const getPagination = (page, size, sort) => {
   const limit = size ? +size : 20;
   const offset = page ? page * limit : 0;
   const sorting = sort ? sort : "desc";
   return {
      limit,
      offset,
      sorting
   };
};
if (typeof web3 !== 'undefined') {
   var web3 = new Web3(web3.currentProvider)
} else {
   var web3 = new Web3(new Web3.providers.HttpProvider(process.env.ETH_NETWORK))
}
var s3  = new AWS.S3({
   accessKeyId: process.env.MINIO_KEY,
   secretAccessKey: process.env.MINIO_SECRET,
   endpoint: process.env.FILE_URL,
   s3ForcePathStyle: true, // needed with minio?
   signatureVersion: 'v4',
   sslEnabled: false,
   rejectUnauthorized: false
});
const increament = async (collection) => {
   const doc = await Counter.findOneAndUpdate({
      name: collection
   }, {
      $inc: {
         seq: 1
      }
   }, {
      new: true,
      upsert: true
   })
   return doc
}
exports.getEtherBalance = async (req, res) => {
   try {
      const address = web3.utils.toChecksumAddress(req.params.id)
      const isAddress = web3.utils.isAddress(address)
      if (!isAddress) {
         return res.status(422).send({
            message: "Not valid address"
         })
      } else {
         const contract = await new web3.eth.Contract(config.ABI, process.env.ETH_CONTRACT_ADDRESS)
         await contract.methods.accountBalance(req.params.id).call().then(data => {
            const ether = Web3.utils.fromWei(data, 'ether');
            const eth = parseFloat(ether)
            const balance = {
               "wei": data,
               "ether": eth
            }
            return res.status(200).send({
               balance
            })
         }).catch(e => {
            return res.status(500).send({
               message: e.message
            })
         })
      }
   } catch (e) {
      return res.status(422).send({
         message: "Not valid address"
      })
   }
}
exports.tokenCount = async (req, res) => {
   const contract = await new web3.eth.Contract(config.ABI, process.env.ETH_CONTRACT_ADDRESS)
   await contract.methods.tokenCount().call().then(data => {
      return res.status(200).send({
         tokenCount: parseInt(data)
      })
   }).catch(e => {
      return res.status(500).send({
         message: e.message
      })
   })
}
exports.balanceOf = async (req, res) => {
   const v = new Validator(req.body, {
      address: 'required',
      index: 'required'
   });
   v.check().then(async matched => {
      if (!matched) {
         return res.status(400).send({
            message: v.errors
         })
      }
      const contract = await new web3.eth.Contract(config.ABI, process.env.ETH_CONTRACT_ADDRESS)
      await contract.methods.balanceOf(req.body.address, req.body.index).call().then(data => {
         return res.status(200).send({
            balance: parseInt(data)
         })
      }).catch(e => {
         return res.status(500).send({
            message: e.message
         })
      })
   }).catch(e => {
      return res.status(500).send({
         message: e.message
      })
   })
}
exports.tokenSync = async (req, res) => {
   try {
      const address = web3.utils.toChecksumAddress(req.body.address)
      const isAddress = web3.utils.isAddress(address)
      if (!isAddress) {
         return res.status(422).send({
            message: "Not valid address"
         })
      } else {
         const v = new Validator(req.body, {
            address: 'required',
            index: 'required',
            priceId: 'required'
         });
         v.check().then(async matched => {
            if (!matched) {
               return res.status(400).send({
                  message: v.errors
               })
            }
            const metadata = req.body.index
            let tokenAmount;
            const contract = await new web3.eth.Contract(config.ABI, process.env.ETH_CONTRACT_ADDRESS)
            await contract.methods.balanceOf(req.body.address, req.body.index).call().then(data => {
               tokenAmount = data
            }).catch(e => {
               return res.status(500).send({
                  message: e.message
               })
            })
            await contract.methods.TokenPriceID(req.body.priceId).call().then(async data => {
               // console.log(data);
               const ether = Web3.utils.fromWei(data.price, 'ether');
               const eth = parseFloat(ether)
               // place for sale
               await Sale.findOneAndUpdate({
                  address: req.body.address,
                  tokenID: req.body.index
               }, {
                  $set: {
                     metadata: metadata,
                     address: req.body.address,
                     tokenID: req.body.index,
                     priceID: req.body.priceId,
                     tokenPriceWei: data.price,
                     tokenPriceEther: eth,
                     isForSale: data.isForSale,
                     amount: tokenAmount
                  }
               }, {
                  upsert: true,
                  new: true
               })
               // update collection
               await Collection.findOneAndUpdate({
                  address: req.body.address,
                  tokenID: req.body.index
               }, {
                  $set: {
                     metadata: metadata,
                     address: req.body.address,
                     tokenID: req.body.index,
                     priceID: req.body.priceId,
                     tokenPriceWei: data.price,
                     tokenPriceEther: eth,
                     isForSale: data.isForSale,
                     amount: tokenAmount
                  }
               }, {
                  upsert: true,
                  new: true
               })
               return res.status(200).send({
                  data: data
               })
            }).catch(e => {
               return res.status(500).send({
                  message: e.message
               })
            })
         }).catch(e => {
            return res.status(500).send({
               message: e.message
            })
         })
      }
   } catch (e) {
      return res.status(422).send({
         message: "Not valid address"
      })
   }
}
exports.newToken = (req, res) => {
   const v = new Validator(req.body, {
      name: 'required',
      description: 'required',
      network: 'required',
      txHash: 'required'
   });
   v.check().then(async matched => {
      if (!matched) {
         return res.status(400).send({
            message: v.errors
         })
      }
      var metaData = {
         'Content-Type': req.file.mimetype
      }
      const name = uuidv4()
      web3.eth.getTransactionReceipt(req.body.txHash, function(error, receipt) {
         if (error) {
            return res.status(404).send({
               message: "Invalid txHash"
            });
         }
         try {
            if (receipt.blockNumber == undefined) return res.status(404).send({
               message: "Transaction receipt not found"
            });
            // save to db
            // Setting up S3 upload parameters
            const params = {
               Bucket: process.env.MINIO_BUCKET,
               Key: name, 
               ContentType: req.file.mimetype,
               Body: req.file.buffer
            };
            s3.putObject(params, function(err, data) {
               if (error) {
                  return res.status(422).send({
                     message: error
                  });
               }
               web3.eth.getBlock(receipt.blockNumber, async (error, block) => {
                  if (error) {
                     return res.status(500).send({
                        message: error.message || "Error"
                     });
                  }
                  const timestamp = block.timestamp;
                  abiDecoder.addABI(config.ABI);
                  const decodedLogs = abiDecoder.decodeLogs(receipt.logs);
                  let dataTx = {
                     "txHash": receipt.transactionHash,
                     "blockNumber": receipt.blockNumber,
                     "from": decodedLogs[0].events[1].value,
                     "to": decodedLogs[0].events[2].value,
                     "metadata": decodedLogs[0].events[3].value,
                     "tokenId": decodedLogs[0].events[3].value,
                     "amount": decodedLogs[0].events[4].value,
                     "priceId": decodedLogs[1].events[3].value,
                     "tokenPrice": decodedLogs[1].events[4].value,
                     "isForSale": decodedLogs[1].events[5].value,
                     "isPurchased": decodedLogs[2].events[4].value,
                     "timestamp": timestamp
                  }
                  const ether = Web3.utils.fromWei(dataTx.tokenPrice, 'ether');
                  const eth = parseFloat(ether)
                  // request body
                  req.body._id = dataTx.tokenId
                  req.body.tokenIdHex = String(decodedLogs[0].events[3].value).padStart(64, '0')
                  req.body.txHash = dataTx.txHash
                  req.body.file = process.env.FILE_URL + '/' + process.env.MINIO_BUCKET + '/' + name
                  req.body.status = receipt.status
                  req.body.creator = decodedLogs[0].events[2].value
                  const metadata = new Metadata(req.body)
                  // place for sale
                  await Sale.findOneAndUpdate({
                     address: decodedLogs[0].events[2].value,
                     tokenID: decodedLogs[0].events[3].value
                  }, {
                     $set: {
                        metadata: dataTx.tokenId,
                        address: decodedLogs[0].events[2].value,
                        tokenID: decodedLogs[0].events[3].value,
                        priceID: dataTx.priceId,
                        tokenPriceWei: dataTx.tokenPrice,
                        tokenPriceEther: eth,
                        isForSale: dataTx.isForSale,
                        amount: dataTx.amount
                     }
                  }, {
                     upsert: true,
                     new: true
                  })
                  // update collection
                  await Collection.findOneAndUpdate({
                     address: decodedLogs[0].events[2].value,
                     tokenID: decodedLogs[0].events[3].value
                  }, {
                     $set: {
                        metadata: dataTx.tokenId,
                        address: decodedLogs[0].events[2].value,
                        tokenID: decodedLogs[0].events[3].value,
                        priceID: dataTx.priceId,
                        tokenPriceWei: dataTx.tokenPrice,
                        tokenPriceEther: eth,
                        isForSale: dataTx.isForSale,
                        isOwner: true,
                        amount: dataTx.amount
                     }
                  }, {
                     upsert: true,
                     new: true
                  })
                  // save tx into db
                  await Tx.findOneAndUpdate({
                     _id: receipt.transactionHash
                  }, {
                     $set: dataTx
                  }, {
                     upsert: true,
                     new: true
                  })
                  metadata.save(function(err, doc) {
                     if (err) {
                        return res.status(500).send({
                           message: err
                        });
                     }
                     return res.status(201).send({
                        doc
                     });
                  });
               });
            });
         } catch (e) {
            return res.status(404).send({
               message: "Invalid tx receipt: " + e
            });
         }
      });
   }).catch(e => {
      return res.status(500).send({
         message: e.message
      })
   })
}
exports.getAll = async (req, res) => {
   const {
      page,
      size
   } = req.query;
   const {
      limit,
      offset
   } = getPagination(page, size);
   const options = {
      offset: offset,
      limit: limit
   };
   Metadata.paginate({}, options).then(data => {
      return res.status(200).send(data);
   }).catch(err => {
      return res.status(500).send({
         message: err.message || "Error"
      });
   });
};
exports.item = (req, res) => {
   Metadata.findOne({
      tokenIdHex: req.params.id
   }).select('-_id -__v -tokenIdHex -tokenId').then(data => {
      if (!data) {
         return res.status(404).send({
            message: "Token not found"
         });
      }
      return res.status(200).send(data);
   }).catch(err => {
      return res.status(500).send({
         message: err.message || "Error"
      });
   });
};
exports.Tx = async (req, res) => {
   web3.eth.getTransactionReceipt(req.body.txHash, function(error, receipt) {
      // console.log(receipt);
      if (error) {
         return res.status(404).send({
            message: "Invalid txHash"
         });
      }
      try {
         if (receipt.blockNumber == undefined) return res.status(404).send({
            message: "Transaction receipt not found"
         });
         web3.eth.getBlock(receipt.blockNumber, async (error, block) => {
            if (error) {
               return res.status(500).send({
                  message: error.message || "Error"
               });
            }
            const timestamp = block.timestamp;
            abiDecoder.addABI(config.ABI);
            const decodedLogs = abiDecoder.decodeLogs(receipt.logs);
            // console.log(decodedLogs[0].events);
            let dataTx;
            if (decodedLogs[0].events[0].value == decodedLogs[0].events[1].value) {
               let dataTxx = {
                  "txHash": receipt.transactionHash,
                  "blockNumber": receipt.blockNumber,
                  "from": decodedLogs[0].events[0].value,
                  "to": decodedLogs[0].events[1].value,
                  "metadata": decodedLogs[0].events[2].value,
                  "tokenId": decodedLogs[0].events[2].value,
                  "amount": parseInt(decodedLogs[0].events[6].value),
                  "priceId": decodedLogs[0].events[3].value,
                  "tokenPrice": decodedLogs[0].events[4].value,
                  "isForSale": decodedLogs[0].events[5].value,
                  "isPurchased": false,
                  "timestamp": timestamp
               }
               dataTx = dataTxx;
            } else {
               let dataTxx = {
                  "txHash": receipt.transactionHash,
                  "blockNumber": receipt.blockNumber,
                  "from": decodedLogs[0].events[1].value,
                  "to": decodedLogs[0].events[2].value,
                  "metadata": decodedLogs[0].events[3].value,
                  "tokenId": decodedLogs[0].events[3].value,
                  "amount": decodedLogs[0].events[4].value,
                  "priceId": decodedLogs[1].events[3].value,
                  "tokenPrice": decodedLogs[1].events[4].value,
                  "isForSale": decodedLogs[1].events[5].value,
                  "isPurchased": decodedLogs[2].events[4].value,
                  "timestamp": timestamp
               }
               dataTx = dataTxx;
            }
            // console.log(dataTx);
            const ether = Web3.utils.fromWei(dataTx.tokenPrice, 'ether');
            const eth = parseFloat(ether)
            if (dataTx.isForSale && !dataTx.isPurchased) {
               const contract = await new web3.eth.Contract(config.ABI, process.env.ETH_CONTRACT_ADDRESS)
               await contract.methods.balanceOf(dataTx.to, dataTx.tokenId).call().then(async data => {
                  const tokenAmount = parseInt(data)
                  // console.log(data);
                  // place for sale
                  await Sale.findOneAndUpdate({
                     address: dataTx.to,
                     tokenID: dataTx.tokenId
                  }, {
                     $set: {
                        metadata: dataTx.tokenId,
                        address: dataTx.to,
                        tokenID: dataTx.tokenId,
                        priceID: dataTx.priceId,
                        tokenPriceWei: dataTx.tokenPrice,
                        tokenPriceEther: eth,
                        isForSale: dataTx.isForSale,
                        amount: tokenAmount
                     }
                  }, {
                     upsert: true,
                     new: true
                  })
                  // update collection
                  await Collection.findOneAndUpdate({
                     address: dataTx.to,
                     tokenID: dataTx.tokenId
                  }, {
                     $set: {
                        metadata: dataTx.tokenId,
                        address: dataTx.to,
                        tokenID: dataTx.tokenId,
                        priceID: dataTx.priceId,
                        tokenPriceWei: dataTx.tokenPrice,
                        tokenPriceEther: eth,
                        isForSale: dataTx.isForSale,
                        amount: dataTx.amount
                     }
                  }, {
                     upsert: true,
                     new: true
                  })
               }).catch(e => {
                  return res.status(500).send({
                     message: e.message
                  });
               })
            }
            if (!dataTx.isForSale && dataTx.isPurchased) {
               const contract = await new web3.eth.Contract(config.ABI, process.env.ETH_CONTRACT_ADDRESS)
               // update buyer balance
               await contract.methods.balanceOf(dataTx.to, dataTx.tokenId).call().then(async data => {
                  const buyerAmount = parseInt(data)
                  // console.log(buyerAmount);
                  await Collection.findOneAndUpdate({
                     address: dataTx.to,
                     tokenID: dataTx.tokenId
                  }, {
                     $set: {
                        metadata: dataTx.tokenId,
                        address: dataTx.to,
                        tokenID: dataTx.tokenId,
                        priceID: dataTx.priceId,
                        tokenPriceWei: dataTx.tokenPrice,
                        tokenPriceEther: eth,
                        isForSale: dataTx.isForSale,
                        isOwner: true,
                        amount: buyerAmount
                     }
                  }, {
                     upsert: true,
                     new: true
                  })
                  await Collection.findOneAndUpdate({
                     address: dataTx.from,
                     tokenID: dataTx.tokenId
                  }, {
                     $set: {
                        isForSale: false,
                        isOwner: false
                     }
                  }, {
                     upsert: true,
                     new: true
                  })
               }).catch(e => {
                  return res.status(500).send({
                     message: e.message
                  });
               })
               // update seller balance
               await contract.methods.balanceOf(dataTx.from, dataTx.tokenId).call().then(async data => {
                  const sellerAmount = parseInt(data)
                  await Collection.findOneAndUpdate({
                     address: dataTx.from,
                     tokenID: dataTx.tokenId
                  }, {
                     $set: {
                        metadata: dataTx.tokenId,
                        address: dataTx.from,
                        tokenID: dataTx.tokenId,
                        amount: sellerAmount
                     }
                  }, {
                     upsert: true,
                     new: true
                  })
                  await Sale.findOneAndUpdate({
                     address: dataTx.from,
                     tokenID: dataTx.tokenId
                  }, {
                     $set: {
                        metadata: dataTx.tokenId,
                        address: dataTx.from,
                        tokenID: dataTx.tokenId,
                        amount: sellerAmount
                     }
                  }, {
                     upsert: true,
                     new: true
                  })
               }).catch(e => {
                  return res.status(500).send({
                     message: e.message
                  });
               })
            }
            // save tx into db
            await Tx.findOneAndUpdate({
               _id: receipt.transactionHash
            }, {
               $set: dataTx
            }, {
               upsert: true,
               new: true
            }).then(data => {
               return res.status(200).send({
                  data
               });
            }).catch(e => {
               return res.status(500).send({
                  message: e.message
               });
            })
         });
      } catch (e) {
         return res.status(404).send({
            message: "Invalid tx receipt: " + e
         });
      }
   });
}
exports.getAllTransaction = async (req, res) => {
   const {
      page,
      size
   } = req.query;
   const {
      limit,
      offset
   } = getPagination(page, size);
   const path = [{
      path: 'metadata',
      select: '-__v'
   }];
   const options = {
      populate: path,
      offset: offset,
      limit: limit,
      select: '-__v'
   };
   Tx.paginate({}, options).then(data => {
      return res.status(200).send(data);
   }).catch(err => {
      return res.status(500).send({
         message: err.message || "Error"
      });
   });
};
exports.getAddressTransactionHistory = async (req, res) => {
   try {
      const address = web3.utils.toChecksumAddress(req.params.account)
      const isAddress = web3.utils.isAddress(address)
      if (!isAddress) {
         return res.status(422).send({
            message: "Not valid address"
         })
      } else {
         const {
            page,
            size,
            sort
         } = req.query;
         const {
            limit,
            offset,
            sorting
         } = getPagination(page, size, sort);
         const path = [{
            path: 'metadata',
            select: '-__v'
         }];
         const options = {
            populate: path,
            offset: offset,
            limit: limit,
            sort: {
               timestamp: sorting
            },
            select: '-__v'
         };
         Tx.paginate({
            $or: [{
               to: req.params.account
            }, {
               from: req.params.account
            }]
         }, options).then(data => {
            return res.status(200).send(data);
         }).catch(err => {
            return res.status(500).send({
               message: err.message || "Error"
            });
         });
      }
   } catch (e) {
      return res.status(422).send({
         message: "Not valid address"
      })
   }
};
exports.getTokenTransactionHistory = async (req, res) => {
   const {
      page,
      size,
      sort
   } = req.query;
   const {
      limit,
      offset,
      sorting
   } = getPagination(page, size, sort);
   const path = [{
      path: 'metadata',
      select: '-__v'
   }];
   const options = {
      populate: path,
      offset: offset,
      limit: limit,
      sort: {
         timestamp: sorting
      },
      select: '-__v'
   };
   Tx.paginate({
      tokenId: req.params.tokenId
   }, options).then(data => {
      return res.status(200).send(data);
   }).catch(err => {
      return res.status(500).send({
         message: err.message || "Error"
      });
   });
};
exports.getAccountCollections = async (req, res) => {
   const {
      page,
      size,
      sort
   } = req.query;
   const {
      limit,
      offset,
      sorting
   } = getPagination(page, size, sort);
   const path = [{
      path: 'metadata',
      select: '-__v'
   }];
   const options = {
      populate: path,
      offset: offset,
      limit: limit,
      sort: {
         timestamp: sorting
      },
      select: '-__v'
   };
   Collection.paginate({
      address: req.params.account,
      isOwner: true
   }, options).then(data => {
      return res.status(200).send(data);
   }).catch(err => {
      return res.status(500).send({
         message: err.message || "Error"
      });
   });
};
exports.getCollectionOnSales = async (req, res) => {
   const {
      page,
      size,
      sort
   } = req.query;
   const {
      limit,
      offset,
      sorting
   } = getPagination(page, size, sort);
   const path = [{
      path: 'metadata',
      select: '-__v'
   }];
   const options = {
      populate: path,
      offset: offset,
      limit: limit,
      sort: {
         ether: sorting,
         timestamp: sorting
      },
      select: '-__v'
   };
   Sale.paginate({
      isForSale: true,
      amount: {
         "$ne": 0
      }
   }, options).then(data => {
      return res.status(200).send(data);
   }).catch(err => {
      return res.status(500).send({
         message: err.message || "Error"
      });
   });
};
exports.manualTx = async (req, res) => {
   abiDecoder.addABI(config.ABI);
   web3.eth.getTransactionReceipt(req.params.tx, function(error, receipt) {
      if (error) {
         return res.status(404).send({
            message: "Invalid txHash"
         });
      }
      try {
         if (receipt.blockNumber == undefined) return res.status(404).send({
            message: "Transaction receipt not found"
         });
      } catch (e) {
         return res.status(404).send({
            message: "Invalid tx receipt: " + e
         });
      }
      web3.eth.getBlock(receipt.blockNumber, async (error, block) => {
         if (error) {
            return res.status(500).send({
               message: error.message || "Error"
            });
         }
         const timestamp = block.timestamp;
         const decodedLogs = abiDecoder.decodeLogs(receipt.logs);
         let dataTx = {
            "_id": receipt.transactionHash,
            "blockNumber": receipt.blockNumber,
            "from": decodedLogs[0].events[1].value,
            "to": decodedLogs[0].events[2].value,
            "tokenId": decodedLogs[0].events[3].value,
            "amount": decodedLogs[0].events[4].value,
            "priceId": decodedLogs[1].events[3].value,
            "tokenPrice": decodedLogs[1].events[4].value,
            "isForSale": decodedLogs[1].events[5].value,
            "isPurchased": decodedLogs[2].events[4].value,
            "timestamp": timestamp
         }
         const ether = Web3.utils.fromWei(dataTx.tokenPrice, 'ether');
         const eth = parseFloat(ether)
         const tokenAmount = parseInt(dataTx.amount)
         dataTx.amount = tokenAmount
         dataTx.tokenPriceEther = eth
         dataTx.metadata = receipt.transactionHash
         if (dataTx.isForSale) {
            const contract = await new web3.eth.Contract(config.ABI, process.env.ETH_CONTRACT_ADDRESS)
            await contract.methods.balanceOf(decodedLogs[0].events[2].value, decodedLogs[0].events[3].value).call().then(async data => {
               dataTx.amount = parseInt(data)
               // place for sale
               await Sale.findOneAndUpdate({
                  address: decodedLogs[0].events[2].value,
                  tokenID: decodedLogs[0].events[3].value
               }, {
                  $set: {
                     metadata: dataTx.metadata,
                     address: decodedLogs[0].events[2].value,
                     tokenID: decodedLogs[0].events[3].value,
                     priceID: dataTx.priceId,
                     tokenPriceWei: dataTx.tokenPrice,
                     tokenPriceEther: eth,
                     isForSale: dataTx.isForSale,
                     amount: dataTx.amount
                  }
               }, {
                  upsert: true,
                  new: true
               })
               // update collection
               await Collection.findOneAndUpdate({
                  address: decodedLogs[0].events[2].value,
                  tokenID: decodedLogs[0].events[3].value
               }, {
                  $set: {
                     metadata: dataTx.metadata,
                     address: decodedLogs[0].events[2].value,
                     tokenID: decodedLogs[0].events[3].value,
                     priceID: dataTx.priceId,
                     tokenPriceWei: dataTx.tokenPrice,
                     tokenPriceEther: eth,
                     isForSale: dataTx.isForSale,
                     amount: dataTx.amount
                  }
               }, {
                  upsert: true,
                  new: true
               })
            }).catch(e => {
               return res.status(500).send({
                  message: e.message
               });
            })
         }
         if (dataTx.isPurchased) {
            const contract = await new web3.eth.Contract(config.ABI, process.env.ETH_CONTRACT_ADDRESS)
            // update buyer balance
            await contract.methods.balanceOf(decodedLogs[0].events[2].value, decodedLogs[0].events[3].value).call().then(async data => {
               dataTx.amount = parseInt(data)
               await Collection.findOneAndUpdate({
                  address: decodedLogs[0].events[2].value,
                  tokenID: decodedLogs[0].events[3].value
               }, {
                  $set: {
                     metadata: dataTx.metadata,
                     address: decodedLogs[0].events[2].value,
                     tokenID: decodedLogs[0].events[3].value,
                     priceID: dataTx.priceId,
                     tokenPriceWei: dataTx.tokenPrice,
                     tokenPriceEther: eth,
                     isForSale: dataTx.isForSale,
                     amount: dataTx.amount
                  }
               }, {
                  upsert: true,
                  new: true
               })
            }).catch(e => {
               return res.status(500).send({
                  message: e.message
               });
            })
            // update seller balance
            await contract.methods.balanceOf(decodedLogs[0].events[2].value, decodedLogs[0].events[3].value).call().then(async data => {
               dataTx.amount = parseInt(data)
               await Collection.findOneAndUpdate({
                  address: decodedLogs[0].events[2].value,
                  tokenID: decodedLogs[0].events[3].value
               }, {
                  $set: {
                     metadata: dataTx.metadata,
                     address: dataTx.from,
                     tokenID: decodedLogs[0].events[3].value,
                     amount: dataTx.amount
                  }
               }, {
                  upsert: true,
                  new: true
               })
            }).catch(e => {
               return res.status(500).send({
                  message: e.message
               });
            })
         }
         // save tx into db
         await Tx.findOneAndUpdate({
            _id: receipt.transactionHash
         }, {
            $set: dataTx
         }, {
            upsert: true,
            new: true
         }).then(data => {
            return res.status(200).send({
               data
            });
         }).catch(e => {
            return res.status(500).send({
               message: e.message
            });
         })
      });
   });
};
exports.sendToken = async (req, res) => {
   abiDecoder.addABI(config.ABI);
   web3.eth.getTransactionReceipt(req.params.txHash, function(error, receipt) {
      if (error) {
         return res.status(404).send({
            message: "Invalid txHash"
         });
      }
      try {
         if (receipt.blockNumber == undefined) return res.status(404).send({
            message: "Transaction receipt not found"
         });
      } catch (e) {
         return res.status(404).send({
            message: "Invalid tx receipt: " + e
         });
      }
      web3.eth.getBlock(receipt.blockNumber, async (error, block) => {
         if (error) {
            return res.status(500).send({
               message: error.message || "Error"
            });
         }
         const timestamp = block.timestamp;
         const decodedLogs = abiDecoder.decodeLogs(receipt.logs);
         let dataTx = {
            "_id": receipt.transactionHash,
            "blockNumber": receipt.blockNumber,
            "from": decodedLogs[0].events[1].value,
            "to": decodedLogs[0].events[2].value,
            "tokenId": decodedLogs[0].events[3].value,
            "amount": decodedLogs[0].events[4].value,
            "priceId": 0,
            "tokenPrice": 0,
            "isForSale": false,
            "isPurchased": false,
            "timestamp": timestamp
         }
         //  console.log(dataTx);
         const contract = await new web3.eth.Contract(config.ABI, process.env.ETH_CONTRACT_ADDRESS)
         await contract.methods.balanceOf(dataTx.from, dataTx.tokenId).call().then(async data => {
            dataTx.amount = parseInt(data)
            // update reciever balance
            await Collection.findOneAndUpdate({
               address: dataTx.to,
               tokenID: dataTx.tokenId
            }, {
               $set: {
                  metadata: dataTx.metadata,
                  address: dataTx.to,
                  tokenID: dataTx.tokenId,
                  amount: decodedLogs[0].events[4].value,
                  priceID: 0,
                  tokenPriceWei: 0,
                  tokenPriceEther: 0,
                  isForSale: false,
               }
            }, {
               upsert: true,
               new: true
            })
            // update sender balance
            await Collection.findOneAndUpdate({
               address: dataTx.from,
               tokenID: dataTx.tokenId
            }, {
               $set: {
                  metadata: dataTx.metadata,
                  address: dataTx.from,
                  tokenID: dataTx.tokenId,
                  amount: dataTx.amount
               }
            }, {
               upsert: true,
               new: true
            })
            // update sender sale
            await Sale.findOneAndUpdate({
               address: dataTx.from,
               tokenID: dataTx.tokenId
            }, {
               $set: {
                  metadata: dataTx.metadata,
                  address: dataTx.from,
                  tokenID: dataTx.tokenId,
                  amount: dataTx.amount
               }
            }, {
               upsert: true,
               new: true
            })
            return res.status(200).send({
               dataTx
            });
         }).catch(e => {
            return res.status(500).send({
               message: e.message
            });
         })
      });
   });
};
exports.getSold = async (req, res) => {
   try {
      const address = web3.utils.toChecksumAddress(req.params.account)
      const isAddress = web3.utils.isAddress(address)
      if (!isAddress) {
         return res.status(422).send({
            message: "Not valid address"
         })
      } else {
         const {
            page,
            size,
            sort
         } = req.query;
         const {
            limit,
            offset,
            sorting
         } = getPagination(page, size, sort);
         const path = [{
            path: 'metadata',
            select: '-__v'
         }];
         const options = {
            populate: path,
            offset: offset,
            limit: limit,
            sort: {
               timestamp: sorting
            },
            select: '-__v'
         };
         Tx.paginate({
            to: {
               $ne: "0x0000000000000000000000000000000000000000"
            },
            from: req.params.account
         }, options).then(data => {
            return res.status(200).send(data);
         }).catch(err => {
            return res.status(500).send({
               message: err.message || "Error"
            });
         });
      }
   } catch (e) {
      return res.status(422).send({
         message: "Not valid address"
      })
   }
};
exports.getBought = async (req, res) => {
   try {
      const address = web3.utils.toChecksumAddress(req.params.account)
      const isAddress = web3.utils.isAddress(address)
      if (!isAddress) {
         return res.status(422).send({
            message: "Not valid address"
         })
      } else {
         const {
            page,
            size,
            sort
         } = req.query;
         const {
            limit,
            offset,
            sorting
         } = getPagination(page, size, sort);
         const path = [{
            path: 'metadata',
            select: '-__v'
         }];
         const options = {
            populate: path,
            offset: offset,
            limit: limit,
            sort: {
               timestamp: sorting
            },
            select: '-__v'
         };
         Tx.paginate({
            to: req.params.account,
            from: {
               $ne: "0x0000000000000000000000000000000000000000"
            }
         }, options).then(data => {
            return res.status(200).send(data);
         }).catch(err => {
            return res.status(500).send({
               message: err.message || "Error"
            });
         });
      }
   } catch (e) {
      return res.status(422).send({
         message: "Not valid address"
      })
   }
};
exports.getMinted = async (req, res) => {
   try {
      const address = web3.utils.toChecksumAddress(req.params.account)
      const isAddress = web3.utils.isAddress(address)
      if (!isAddress) {
         return res.status(422).send({
            message: "Not valid address"
         })
      } else {
         const {
            page,
            size,
            sort
         } = req.query;
         const {
            limit,
            offset,
            sorting
         } = getPagination(page, size, sort);
         const path = [{
            path: 'metadata',
            select: '-__v'
         }];
         const options = {
            populate: path,
            offset: offset,
            limit: limit,
            sort: {
               timestamp: sorting
            },
            select: '-__v'
         };
         Tx.paginate({
            to: req.params.account,
            from: "0x0000000000000000000000000000000000000000"
         }, options).then(data => {
            return res.status(200).send(data);
         }).catch(err => {
            return res.status(500).send({
               message: err.message || "Error"
            });
         });
      }
   } catch (e) {
      return res.status(422).send({
         message: "Not valid address"
      })
   }
};