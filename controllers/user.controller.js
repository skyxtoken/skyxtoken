const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { Validator } = require('node-input-validator');
const md = require("../models");
const find = require("../helper");
const { authJwt } = require("../middlewares");
const config = require("../config/db.config.js");
const User = md.user;


const getPagination = (page, size) => {
  const limit = size ? +size : 20;
  const offset = page ? page * limit : 0;
  return { limit, offset };
};

exports.signup = (req, res) => {
  const v = new Validator(req.body, {
    email: 'required|email',
    password: 'required'
  });
  v.check().then((matched) => {
    if (!matched) {
      return res.status(422).send(v.errors);
    }
    User.findOne({
        email: req.body.email
    })
    .exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }
      if (user) {
        return res.status(404).send({ message: "User is already exist." });
      }
      const v = new Validator(req.body, {
        email: 'required|email',
        password: 'required'
      });
      v.check().then((matched) => {
        if (!matched) {
          return res.status(422).send(v.errors);
        }
        const user = new User({
          email: req.body.email,
          password: bcrypt.hashSync(req.body.password, 8)
        });
      
        user.save(err => {
          if (err) {
            return res.status(500).send({ message: err });
          }
          return res.status(201).send({ message: "User was registered successfully!" });
        });
      })
    })
  })
};
