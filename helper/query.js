const db = require("../models");
const Organization = db.organization;

getOrganization = (email) => {
  return new Promise(
    (resolve,reject)=>
      Organization
      .findOne({ email: email })
      .exec((err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      })
  );
}

isActiveOrganization = (id) => {
  return new Promise(
    (resolve,reject)=>
      Organization
      .findOne({ 
        _id: id, 
        is_active: true, 
        is_suspended: false 
      })
      .exec((err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      })
  );
}

const query = {
  getOrganization,
  isActiveOrganization
};

module.exports = query;