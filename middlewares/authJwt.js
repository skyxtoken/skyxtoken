const jwt = require("jsonwebtoken");
const config = require("../config/db.config.js");
const db = require("../models");
const Organization = db.organization;
const Mod = db.module;
const User = db.user;
const Feature = db.feature;

findRoles = (arr1, arr2) => { 
  return arr1.some(item => arr2.includes(item)) 
}

verifyOrganizationToken = (req, res, next) => {
  let token = req.headers["x-access-token"];
  if (!token) {
    return res.status(403).send({ message: "No token provided!" });
  }
  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({ message: "Unauthorized!" });
    }
    req.userId = decoded.id;
    req.orgId = decoded.organization
    next();

  })
};

verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];
  if (!token) {
    return res.status(403).send({ message: "No token provided!" });
  }
  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({ message: "Unauthorized!" });
    }
    User.findOne({
        _id: decoded.id,
        organization: decoded.organization,
        branch: decoded.branch,
        is_activated: true,
        is_suspended: false 
    })
    .populate("roles")
    .exec((err, user) => {
      if (err) {
        return res.status(500).send({ message: err });
      }
      if (!user) {
        return res.status(404).send({ message: "User is not found." });
      }
      const roles = [];
      const access = []
      for (let i = 0; i < user.roles.length; i++) {
        roles.push(user.roles[i].slug);
        access.push(user.roles[i]._id);
      }
      req.userId = decoded.id;
      req.orgId = decoded.organization
      req.branchId = decoded.branch
      req.roles = roles
      req.access = access
      next();
    })
  })
};

isAuthorized = (req, res, next) => {
  const url = req.originalUrl
  const urlArray = url.split("/")
  const module = urlArray[2].split("?")
  Organization.findOne(
    {
      _id: req.orgId
    },
    (err, organization) => {
      if (err) {
        return res.status(500).send({ message: err });
      }
      if (!organization) {
        return res.status(404).send({ message: "Organization is not found." });
      }
      if (!organization.is_active) {
        return res.status(400).send({ message: "Organization is not active." });
      }
      if (organization.is_suspended) {
        return res.status(400).send({ message: "Organization is suspended. Please contact our support." });
      }
      Feature.findOne(
        {
          slug: module[0],
          is_active: true,
          is_under_maintenance: false
        },
        (err, feature) => {
          if (err) {
            return res.status(500).send({ message: err });
          }
          if (!feature) {
            return res.status(400).send({ message: "Module is under maintenance." });
          }
          Mod.findOne(
            {
              name: feature._id,
              organization: req.orgId,
              branch: req.branchId
            })
            .populate('GET POST PUT PATCH DELETE', '_id slug')
            .exec(function (err, roles) {
              if (err) {
                res.status(500).send({ message: err });
                return;
              }
              if (roles) {
                const method = req.method
                const authorities = [];
                for (let i = 0; i < roles[method].length; i++) {
                  authorities.push(roles[method][i].slug);
                }
                const isAuthorized = findRoles(authorities, req.roles)
                if(!isAuthorized){
                  return res.status(401).send({ message: "Not authorized." });
                }
                req.featureId = feature._id
                next();
              } else {
                return res.status(400).send({ message: "Module is not found in your organization." });
              }
            }
          )
        }
      )    
    }
  )
};

isModuleOk = (req, res, next) => {
  const url = req.originalUrl
  const urlArray = url.split("/")
  const module = urlArray[2].split("?")
  const modname = []
  Mod.findOne(
    {
      name: req.featureId,
      organization: req.orgId,
      branch: req.branchId
    })
    .populate('name')
    .exec(function (err, mod) {
      if (err) {
        return res.status(500).send({ message: err });
      }
      if (!mod) {
        return res.status(400).send({ message: 'Module not found in your organization.' });
      }
      modname.push(mod.name.slug)
      const moduleCheck = findRoles(modname, module)
      if (!moduleCheck) {
        return res.status(400).send({ message: 'Module not found in your organization.' });
      }
      next();
    }
  );
};

isModulePublic = (req, res, next) => {
  const url = req.originalUrl
  const urlArray = url.split("/")
  const module = urlArray[2].split("?")
  const modname = []
  Mod.find(
    {
      organization: req.orgId,
      branch: req.branchId
    })
    .populate('name')
    .exec(function (err, mod) {
      if (err) {
        return res.status(500).send({ message: err });
      }
      if (!mod) {
        return res.status(400).send({ message: 'Module not found in your organization.' });
      }
      mod.map(item => {
        modname.push(item.name.slug)
      })
      const moduleCheck = findRoles(modname, module)
      if (!moduleCheck) {
        return res.status(400).send({ message: 'Module not found in your organization.' });
      }
      next();
    }
  );
};

const authJwt = {
  findRoles,
  verifyOrganizationToken,
  verifyToken,
  isAuthorized,
  isModuleOk,
  isModulePublic
};
module.exports = authJwt;