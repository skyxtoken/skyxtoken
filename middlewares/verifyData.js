const db = require("../models");
const User = db.user;
const Organization = db.organization;
const Branch = db.branch;

checkDuplicateEmailOrPhone = (req, res, next) => {
  // Email
  User.findOne({
    email: req.body.email
  }).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    if (user) {
      res.status(400).send({ message: "Email is already in use!" });
      return;
    }
    // Phone number
    User.findOne({
      phone_number: req.body.phone_number
    }).exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }
      if (user) {
        res.status(400).send({ message: "Phone number is already in use!" });
        return;
      }
      next();
    });
  });
};

checkOrganization = (req, res, next) => {
  Organization.findOne({
    _id: req.body.organization
  }).exec((err, organization) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    if (!organization) {
      res.status(400).send({ message: "Organization not found" });
      return;
    }
    next();
  });
};

const verifyData = {
  checkDuplicateEmailOrPhone,
  checkOrganization
};

module.exports = verifyData;