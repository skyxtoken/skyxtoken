const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  name: {
    type: String, 
    required: true
  },
  seq: { 
    type: Number, 
    default: 0 
  }
})

const Counter = mongoose.model("Counter", schema);
module.exports = Counter;