const dbConfig = require("../config/db.config.js");
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = dbConfig.url;

db.user = require("./user.model.js");
db.metadata = require("./metadata.model.js");
db.counter = require("./counter.model.js");
db.transaction = require("./transaction.model.js");
db.sale = require("./sale.model.js");
db.collection = require("./collection.model.js");
module.exports = db;