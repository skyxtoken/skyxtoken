const mongoose = require("mongoose");
const validate = require('mongoose-validator')
const mongoosePaginate = require('mongoose-paginate-v2');

const emailValidator = [
    validate({
      validator: 'isLength',
      arguments: [11, 70],
      message: 'email should be between {ARGS[0]} and {ARGS[1]} characters',
    }),
    validate({
      validator: 'isEmail',
      passIfEmpty: true,
      message: 'email should has valiid format',
    })
  ]

const schema = new mongoose.Schema({
  _id: {
    type:Number,
    required:true
  },
  tokenIdHex: {
    type:String,
    required:true
  },
  creator: {
    type:String,
    required:true
  },
  name: {
      type:String,
      required:true
  },
  description: {
    type: String,
    required: true
  },
  file: {
    type:String,
    required:true
  },
  attributes: [],
  network: {
    type: String,
    enum : ['bsc','eth'],
    required:true
  },
  status: {
    type:Boolean
  },
  txHash: {
    type:String,
    required:true
  }
})

schema.plugin(mongoosePaginate);
const Metadata = mongoose.model("Metadata", schema);
module.exports = Metadata;