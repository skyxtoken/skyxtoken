const mongoose = require("mongoose");
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = new mongoose.Schema({
  // _id: {
  //   type: String,
  //   required:true
  // },
  metadata: {
    type: Number, 
    ref: "Metadata"
  },
  address: {
    type:String,
    required:true
  },
  tokenID: {
    type: String,
    required:true
  },
  priceID: {
    type: String
  },
  tokenPriceWei: {
    type:String,
    required:true
  },
  tokenPriceEther: {
    type:Number,
    required:true
  },
  isForSale: {
    type:Boolean,
    required:true
  },
  amount: {
    type:Number
  }
})

schema.plugin(mongoosePaginate);
const Sale = mongoose.model("Sale", schema);
module.exports = Sale;