const mongoose = require("mongoose");
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = new mongoose.Schema({
  _id: {
    type:String,
    required:true
  },
  metadata: {
    type: Number, 
    ref: "Metadata",
    required: true
  },
  blockNumber: {
    type:Number,
    required:true
  },
  from: {
    type: String,
    required: true
  },
  to: {
    type:String,
    required:true
  },
  tokenId: {
    type: String,
    required:true
  },
  amount: {
    type:Number,
    required:true
  },
  priceId: {
    type:String,
    required:true
  },
  tokenPrice: {
    type:String,
    required:true
  },
  tokenPriceEther: {
    type:Number,
    required:true
  },
  isForSale: {
    type:Boolean,
    required:true
  },
  timestamp: {
    type:Number,
    required:true
  }
})

schema.plugin(mongoosePaginate);
const Transaction = mongoose.model("Transaction", schema);
module.exports = Transaction;