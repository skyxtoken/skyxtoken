const mongoose = require("mongoose");
const validate = require('mongoose-validator')
const mongoosePaginate = require('mongoose-paginate-v2');

const emailValidator = [
    validate({
      validator: 'isLength',
      arguments: [11, 70],
      message: 'email should be between {ARGS[0]} and {ARGS[1]} characters',
    }),
    validate({
      validator: 'isEmail',
      passIfEmpty: true,
      message: 'email should has valiid format',
    })
  ]

const schema = new mongoose.Schema({
  email: {
      type:String,
      required:true,
      validate: emailValidator
  },
  password: {
    type: String,
    required: true
  }
}, { timestamps: true })

schema.plugin(mongoosePaginate);
const User = mongoose.model("User", schema);
module.exports = User;