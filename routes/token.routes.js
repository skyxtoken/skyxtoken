const router = require("express").Router()
const multer  = require('multer')
const storage = multer.memoryStorage()
const upload = multer({ storage: storage })
const token = require("../controllers/token.controller.js")

module.exports = app => {
    router.get("/manual-tx/:tx", token.manualTx);
    router.get("/balance/:id", token.getEtherBalance);
    router.get("/token-count", token.tokenCount);
    router.post("/address-balance", token.balanceOf);
    router.post("/token-sync", token.tokenSync);
    router.post("/new-item", upload.single('token'), token.newToken);
    router.get("/items", token.getAll);
    router.get("/item/:id"+".json", token.item);
    router.post("/tx/new", token.Tx);
    router.get("/tx/all", token.getAllTransaction);
    router.get("/tx/address/:account", token.getAddressTransactionHistory);
    router.get("/tx/token/:tokenId", token.getTokenTransactionHistory);
    router.get("/token/collection/:account", token.getAccountCollections);
    router.get("/token/sale", token.getCollectionOnSales);
    router.get("/send-token/:txHash", token.sendToken);
    router.get("/sold/:account", token.getSold);
    router.get("/bought/:account", token.getBought);
    router.get("/minted/:account", token.getMinted);
    app.use('/api', router);
};