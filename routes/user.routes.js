const router = require("express").Router();
const { verifyData, authJwt } = require("../middlewares");
const user = require("../controllers/user.controller.js");

module.exports = app => {
    router.post("/signup", user.signup);
    app.use('/v1/user', router);
};